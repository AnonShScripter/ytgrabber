# ytgrabber

## a shell script to search for youtube videos, users and playlists to ease download and consumption

## note

this script will *NOT* work on systems with BSD tools, get GNU grep

## dependencies and needs

- bash, obviously
- tput --> ncurses
- GNU grep with PCRE support 

## features

- colorful debug output
- easy customization and editing due to shell script nature
- outputs URLs, simply use command expansion with `head` and you are good to go
- supports multiple search terms
- relatively fast because it doesn't use youtube-dl
- slower than it needs to be because I am a lazy fuck and cannot be fucked to update the process substitutions and extensive usage of grep and its PCRE mode
- an absolutely obnoxious coding style with strict quoting and extensive parameter expansion use 
- generally very scuffed

## usage

```
./ytgrabber <0 1 2, video playlist channel respectively> <search1> [search2, ...]
```

